package math3d;

import io.PropertyValueGet;
import math2d.Circle;

public class Cone extends Circle implements Shape3D {
    private double height;

    public Cone(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @PropertyValueGet(propertyName = "height")
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calculateVolume() {
        return (1.0 / 3) * Math.PI * this.getRadius() * this.getRadius() * this.getHeight();
    }

    @Override
    public String toString() {
        return "Cone{" + "radius=" + this.getRadius() + ", height=" + this.getHeight() + '}';
    }
}
