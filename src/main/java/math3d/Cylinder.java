package math3d;

import io.PropertyValueGet;
import math2d.Circle;

public class Cylinder extends Circle implements Shape3D {
    private double height;

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @PropertyValueGet(propertyName = "height")
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calculateVolume() {
        return (22 * this.getRadius() * this.getRadius() * this.getHeight()) / 7;
    }

    @Override
    public String toString() {
        return "Cylinder{" + "radius=" + this.getRadius() + ", height=" + this.getHeight() + '}';
    }
}
