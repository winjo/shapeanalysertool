package math3d;

import math2d.Triangle;

public class Rhombicosidodecahedron extends Triangle implements Shape3D {

    public Rhombicosidodecahedron(double edge) {
        super(edge);
    }

    @Override
    public double calculateVolume() {
        return (Math.pow(edge, 3) / 3) * (60 + 29 * Math.sqrt(5));
    }

    @Override
    public String toString() {
        return "Rhombicosidodecahedron{" + "edge=" + edge + '}';
    }
}
