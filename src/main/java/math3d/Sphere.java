package math3d;

import math2d.Circle;

public class Sphere extends Circle implements Shape3D {

    public Sphere(double radius) {
        super(radius);
    }

    @Override
    public double calculateVolume() {
        return 3d / 4 * Math.PI * this.getRadius() * this.getRadius() * this.getRadius();
    }

    @Override
    public String toString() {
        return "Sphere{" + "radius=" + this.getRadius() + '}';
    }
}
