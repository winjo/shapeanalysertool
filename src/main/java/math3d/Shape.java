package math3d;

import java.io.Serializable;
import java.util.UUID;

public abstract class Shape implements Serializable {
    protected UUID uuid;
    protected Shape() {
        this.uuid = UUID.randomUUID();
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
