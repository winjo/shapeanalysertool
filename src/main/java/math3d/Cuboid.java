package math3d;

import io.PropertyValueGet;
import math2d.Rectangle;

public class Cuboid extends Rectangle implements Shape3D {
    private double height;

    public Cuboid(double width, double length, double height) {
        super(width, length);
        this.height = height;
    }

    @PropertyValueGet(propertyName = "height")
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calculateVolume() {
        return this.getWidth() * this.getLength() * this.getHeight();
    }

    @Override
    public String toString() {
        return "Cuboid{" + "width=" + this.getWidth() + ", length=" + this.getLength() + ", height=" + this.getHeight() + '}';
    }
}
