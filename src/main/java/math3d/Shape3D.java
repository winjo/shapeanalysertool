package math3d;

import java.util.UUID;

public interface Shape3D {
    /**
     * @return calculated volume
     */
    double calculateVolume();

    UUID getUuid();
}
