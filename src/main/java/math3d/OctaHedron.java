package math3d;

import math2d.Triangle;

public class OctaHedron extends Triangle implements Shape3D {

    public OctaHedron(double edge) {
        super(edge);
    }

    @Override
    public double calculateVolume() {
        return ((edge * edge * edge) * (Math.sqrt(2) / 3));
    }

    @Override
    public String toString() {
        return "OctaHedron{" + "edge=" + edge + '}';
    }
}
