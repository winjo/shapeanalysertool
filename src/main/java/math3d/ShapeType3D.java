package math3d;

public enum ShapeType3D {
    CONE,
    CUBOID,
    CYLINDER,
    OCTAHEDRON,
    RHOMBICOSIDODECAHEDRON,
    SPHERE
}
