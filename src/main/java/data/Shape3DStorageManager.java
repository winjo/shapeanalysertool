package data;

import io.ShapeReader;
import io.ShapeWriter;
import math3d.Shape3D;

import javax.swing.event.EventListenerList;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * The Shape3DStorageManager holds all the Shape3D objects in a list with the Shape3D generic type.
 * A storage manager was chosen for easy volume calculation for the list of objects, also for the implementation
 * of the ShapeReader and ShapeWriter interfaces. This results in a more generic approach.
 */
public class Shape3DStorageManager {
    private List<Shape3D> shape3DList = new ArrayList<>();
    protected ArrayList<Consumer> eventListeners = new ArrayList<>();

    public void addShape(Shape3D shape) {
        this.shape3DList.add(shape);
        notifyChanged();
    }

    public void addShapes(List<Shape3D> shape3DList) {
        this.shape3DList.addAll(shape3DList);
    }

    public void deleteShape(Shape3D shape) {
        this.shape3DList.remove(shape);
        notifyChanged();
    }

    public void deleteShape(int shapeIndex) {
        this.shape3DList.remove(shapeIndex);
        notifyChanged();
    }

    public void deleteShapes(List<Shape3D> shape3DList) {
        this.shape3DList.removeAll(shape3DList);
        notifyChanged();
    }

    public void deleteAllShapes(){
        shape3DList.clear();
        notifyChanged();
    }

    public double calculateTotalVolume() {
        double total = 0.0;
        for (Shape3D shape : shape3DList) {
            total += shape.calculateVolume();
        }
        return total;
    }

    public List<Shape3D> getShape3DList() {
        return shape3DList;
    }

    public void setShape3DList(List<Shape3D> storedShapes) {
        this.shape3DList = storedShapes;
        notifyChanged();
    }

    public void setShape3DList(ShapeReader shapeReader) {
        List<Shape3D> shape3DList = shapeReader.getShapes();

        if (shape3DList != null) {
            this.shape3DList = shape3DList;
            notifyChanged();
        }
    }

    public void saveShapes(ShapeWriter shapeWriter) {
        shapeWriter.saveShapes(this.shape3DList);
    }

    public void addActionListener(Consumer predicate)
    {
        eventListeners.add(predicate);
    }

    private void notifyChanged()
    {
        eventListeners.forEach(x -> x.accept(null));
    }
}