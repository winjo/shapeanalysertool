package io;

import math3d.Shape3D;

import java.util.List;

public interface ShapeReader {
    List<Shape3D> getShapes();
}
