package io;

import math3d.Shape3D;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class ShapeWriterObject implements ShapeWriter {
    @Override
    public boolean saveShapes(List<Shape3D> shape3DList) {
        IOEssential ioEssential = new IOEssential(IOEssential.DIALOG_TYPE.SAVE);
        String filePath = ioEssential.getFilePath("obj");

        if (filePath.isEmpty()) {
            return false;
        }

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {
            objectOutputStream.writeObject(shape3DList);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
