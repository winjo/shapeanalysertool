/*
    Before you start digging into this file, I just want to say, I'm sorry.
    This mess is the biggest mess I've ever written, and hopefully ever will.
    Next project we're going to think this through if we decide to store these properties into a database.
 */

package io;

import math3d.*;
import settings.appSettings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class ShapeReaderMySql implements ShapeReader
{

    private Connection conn;

    public ShapeReaderMySql()
    {
        try
        {
            // Setting the driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Getting the driver
            conn = DriverManager.getConnection(appSettings.getMySql_host(), appSettings.getMySql_user(), appSettings.getMySql_password());
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Shape3D> getShapes()
    {
        List<SimpleShapeProperty> retval = new ArrayList<>();

        String query = "select s.ShapeType, sp.*\n" +
                "from Shape s\n" +
                "inner join ShapeProperty sp on s.Uuid = sp.ShapeUuid\n" +
                "order by s.Uuid;";

        try
        {
            Statement s = conn.createStatement();
            ResultSet res = s.executeQuery(query);

            while (res.next())
            {
                // Mapping to a simple property
                SimpleShapeProperty ss = new SimpleShapeProperty();
                ss.shapeType = res.getString(1);
                ss.setUuid(UUID.fromString(res.getString(2)));
                ss.propertyName = res.getString(3);
                ss.propertyValue = res.getDouble(4);
                retval.add(ss);
            }

            // Reformats all the properties to it's corresponding class.
            return reformatList(retval);

        } catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return null;
    }

    private static List<Shape3D> reformatList(List<SimpleShapeProperty> input)
    {
        HashMap<UUID, ArrayList<SimpleShapeProperty>> tmp = new HashMap<>();

        // Grouping all the properties based on UUID
        for (SimpleShapeProperty ss : input)
        {
            if (!tmp.containsKey(ss.getUuid()))
            {
                tmp.put(ss.getUuid(), new ArrayList<>());
            }

            tmp.get(ss.getUuid()).add(ss);
        }

        ArrayList<Shape3D> retval = new ArrayList<>();

        // Actually getting the classes responsible for the properties.
        tmp.forEach((k, v) ->
        {
            if (v.size() > 0)
            {
                retval.add(toShape(v));
            }
        });

        return retval;
    }

    // Maps to the corresponding sub class
    private static Shape3D toShape(ArrayList<SimpleShapeProperty> simpleList)
    {
        SimpleShapeProperty first = simpleList.get(0);

        switch (first.shapeType)
        {
            case "OctaHedron":
                return new OctaHedron(getValue("edge", simpleList));
            case "Cylinder":
                return new Cylinder(getValue("radius", simpleList), getValue("height", simpleList));
            case "Cone":
                return new Cone(getValue("radius", simpleList), getValue("height", simpleList));
            case "Rhombicosidodecahedron":
                return new Rhombicosidodecahedron(getValue("edge", simpleList));
            case "Sphere":
                return new Sphere(getValue("radius", simpleList));
            case "Cuboid":
                return new Cuboid(getValue("width", simpleList), getValue("length", simpleList), getValue("height", simpleList));
            default:
                return null;
        }
    }

    // Gets the property by name.
    private static double getValue(String propertyName, ArrayList<SimpleShapeProperty>simpleList){
        for (SimpleShapeProperty simpleShape : simpleList)
        {
            if (propertyName.equals(simpleShape.propertyName)){
                return simpleShape.propertyValue;
            }
        }

        return 0;
    }

    private class SimpleShapeProperty extends Shape
    {
        private String shapeType;

        private String propertyName;

        private double propertyValue;
    }
}
