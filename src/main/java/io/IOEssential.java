package io;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * A user wants to know and perhaps want to change the export/import location of the shape list.
 * To allow the user to do this we have to instantiate a JFileChooser in multiple files.
 * Since this is getting used in more than 2 files we have to follow the DRY principle for cleaner code.
 * Thus this class is created for cleaner code.
 */
public class IOEssential {
    public enum DIALOG_TYPE {
        OPEN,
        SAVE
    }

    private final String DEFAULT_FILTER_EXTENSION = "txt";
    private final String DEFAULT_FILTER_DESCRIPTION = " file";
    private final String DEFAULT_CANCEL_DIALOG_TEXT = "No file selected.";
    private DIALOG_TYPE dialogType;

    /**
     * Overriding the default JSwing UI to match the operating system UI.
     */
    public IOEssential(DIALOG_TYPE dialogType) {
        try {
            this.dialogType = dialogType;
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException ignored) {
        }
    }

    public String getFilePath() {
        return getFilePath(
                DEFAULT_FILTER_EXTENSION,
                DEFAULT_FILTER_EXTENSION + DEFAULT_FILTER_DESCRIPTION, DEFAULT_CANCEL_DIALOG_TEXT);
    }

    public String getFilePath(String filterExtension) {
        return getFilePath(filterExtension, filterExtension + DEFAULT_FILTER_DESCRIPTION, DEFAULT_CANCEL_DIALOG_TEXT);
    }

    public String getFilePath(String filterExtension, String filterDescription) {
        return getFilePath(filterExtension, filterDescription, DEFAULT_CANCEL_DIALOG_TEXT);
    }

    public String getFilePath(String filterExtension, String filterDescription, String cancelDialogText) {
        // TODO enhance JFileChooser with setDialogTitle
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(filterDescription, filterExtension);
        fileChooser.setFileFilter(filter);
        fileChooser.setAcceptAllFileFilterUsed(true);
        int returnVal; // Initialize to prevent Java errors

        if (this.dialogType == DIALOG_TYPE.SAVE) {
            returnVal = fileChooser.showSaveDialog(null);
        } else {
            returnVal = fileChooser.showOpenDialog(null);
        }

        if (returnVal != JFileChooser.APPROVE_OPTION) {
            JOptionPane.showMessageDialog(null, cancelDialogText);
        }

        if (fileChooser.getSelectedFile() == null) {
            return "";
        }

        String filePath = fileChooser.getSelectedFile().getAbsolutePath();

        if (!filePath.endsWith(filterExtension)) {
            filePath += '.' + filterExtension;
        }

        return filePath;
    }
}
