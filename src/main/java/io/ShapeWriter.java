package io;

import math3d.Shape;
import math3d.Shape3D;

import java.util.List;

public interface ShapeWriter {
    boolean saveShapes(List<Shape3D> shape3DList);
}
