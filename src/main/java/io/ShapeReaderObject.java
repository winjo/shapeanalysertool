package io;

import math3d.Shape3D;

import java.io.*;
import java.util.List;

public class ShapeReaderObject implements ShapeReader {
    @Override
    public List<Shape3D> getShapes() {
        IOEssential ioEssential = new IOEssential(IOEssential.DIALOG_TYPE.OPEN);
        String filePath = ioEssential.getFilePath("obj");
        List<Shape3D> shape3DList = null;

        if (filePath.isEmpty()) {
            return shape3DList; // Return null
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(filePath))) {
            // If object is not type of Shape3D then it will become null
            shape3DList = (List<Shape3D>) objectInputStream.readObject();

            return shape3DList;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return shape3DList;
    }
}
