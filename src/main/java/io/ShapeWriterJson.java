package io;

import com.google.gson.Gson;
import math3d.Shape3D;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ShapeWriterJson implements ShapeWriter {
    @Override
    public boolean saveShapes(List<Shape3D> shape3DList) {
        IOEssential abstractIO = new IOEssential(IOEssential.DIALOG_TYPE.SAVE);
        String filePath = abstractIO.getFilePath("json");

        // Do we have a file path?
        if (filePath.isEmpty()) {
            return false;
        }

        try (FileWriter fileWriter = new FileWriter(filePath)) {
            Gson gson = GsonShape3DListAdapterFactory.getShape3DListGson();
            String json = gson.toJson(shape3DList);
            fileWriter.write(json);
            fileWriter.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
