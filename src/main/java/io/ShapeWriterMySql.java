package io;

import math3d.Shape;
import math3d.Shape3D;

import settings.appSettings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.ClosedWatchServiceException;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class ShapeWriterMySql implements ShapeWriter
{

    private Connection conn;

    public ShapeWriterMySql()
    {
        try
        {
            // Setting the driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Getting the driver
            conn = DriverManager.getConnection(appSettings.getMySql_host(), appSettings.getMySql_user(), appSettings.getMySql_password());
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean saveShapes(List<Shape3D> shape3DList)
    {
        // Queries
        String trunceteQuery = "delete from ShapeProperty; delete from Shape";
        String query1 = "insert into Shape (Uuid, ShapeType) values (?, ?)";
        String query2 = "insert into ShapeProperty (ShapeUuid, PropertyName, PropertyValue) values(?,?,?)";

        try
        {
            // Making sure not to commit right away. We want to be sure that both queries succeeded.
            conn.setAutoCommit(false);
            // Wiping table before insert.
            conn.prepareStatement("delete from ShapeProperty;").execute();
            conn.prepareStatement("delete from Shape;").execute();
            PreparedStatement s1 = conn.prepareStatement(query1);
            PreparedStatement s2 = conn.prepareStatement(query2);

            for (Shape3D shape3D : shape3DList)
            {
                // Adding parameters
                SimpleShape simple = new SimpleShape(shape3D);
                s1.setString(1, simple.getUuid().toString());
                s1.setString(2, simple.getShapeClassName());
                s1.addBatch();

                HashMap<String, Double> props = simple.getProperties();
                // Adding parameters for other table
                for (Map.Entry<String, Double> entry : props.entrySet())
                {
                    System.out.println(simple.getShapeClassName());
                    Double v = entry.getValue();
                    String k = entry.getKey();

                    s2.setString(1, simple.getUuid().toString());
                    s2.setString(2, k);
                    s2.setDouble(3, v);
                    s2.addBatch();
                }
            }

            // Executing the queries
            s1.executeBatch();
            s2.executeBatch();

            // Now we can commit, as we are sure the queries did not fail.
            conn.commit();
            return true;
        } catch (SQLException e)
        {
            System.out.println("Failed execute query");

            e.printStackTrace();
        }

        return false;
    }

    /*
    Represents a simple way to describe a shape. This class is mainly used to insert and select shapes to and from the database.
     */
    private class SimpleShape extends Shape
    {
        private String shapeClassName;

        private HashMap<String, Double> properties;

        public SimpleShape(Shape3D shape)
        {
            uuid = shape.getUuid();
            Class<?> cls = shape.getClass();
            shapeClassName = cls.getSimpleName();
            Method[] methods = cls.getMethods();
            properties = new HashMap<>();
            for (Method method : methods)
            {
                if (method.isAnnotationPresent(PropertyValueGet.class))
                {
                    try
                    {
                        double value = (double) method.invoke(shape);
                        properties.put(method.getAnnotation(PropertyValueGet.class).propertyName(), value);
                    } catch (Exception ex)
                    {
                        // ignored
                    }
                }
            }
        }

        public String getShapeClassName()
        {
            return shapeClassName;
        }

        public HashMap<String, Double> getProperties()
        {
            return properties;
        }
    }
}