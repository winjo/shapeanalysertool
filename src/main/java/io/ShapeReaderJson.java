package io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import math3d.*;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class ShapeReaderJson implements ShapeReader {
    @Override
    public List<Shape3D> getShapes() {
        IOEssential ioEssential = new IOEssential(IOEssential.DIALOG_TYPE.OPEN);
        String filePath = ioEssential.getFilePath("json");
        List<Shape3D> shape3DList = null;

        if (filePath.isEmpty()) {
            return shape3DList; // Return null
        }

        try (FileReader fileReader = new FileReader(filePath)) {
            Gson gson = GsonShape3DListAdapterFactory.getShape3DListGson();
            JsonReader jsonReader = new JsonReader(fileReader);
            Type type = new TypeToken<List<Shape3D>>() {
            }.getType();
            shape3DList = gson.fromJson(jsonReader, type);
            // If object is not type of Shape3D then it will become null
            return shape3DList;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return shape3DList;
    }
}
