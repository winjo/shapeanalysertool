package io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import math3d.*;

/**
 * Creating the same factory over and over is not a good practice. To also keep code as much as possible in one place
 * this factory has been created.
 * The RuntimeTypeAdapterFactory is a modified factory from the GSON dependency that is meant for generic type collections.
 */
public class GsonShape3DListAdapterFactory {
    public static Gson getShape3DListGson() {
        RuntimeTypeAdapterFactory<Shape3D> shape3DAdapterFactory = RuntimeTypeAdapterFactory.of(Shape3D.class, "shape3D");
        shape3DAdapterFactory
                .registerSubtype(Cone.class)
                .registerSubtype(Cuboid.class)
                .registerSubtype(Cylinder.class)
                .registerSubtype(OctaHedron.class)
                .registerSubtype(Rhombicosidodecahedron.class)
                .registerSubtype(Sphere.class);
        return new GsonBuilder().registerTypeAdapterFactory(shape3DAdapterFactory).create();
    }
}
