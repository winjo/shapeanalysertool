package io;

import math3d.Shape3D;

import java.io.FileNotFoundException;
import java.util.List;

public class ShapeReaderWriter implements ShapeReader, ShapeWriter
{
    private ShapeReader reader;
    private ShapeWriter writer;

    public ShapeReaderWriter(ShapeReader reader, ShapeWriter writer)
    {
        System.out.println("Created a new instance of ShapeReaderWriter with reader: " + reader.getClass() + " and writer: " + writer.getClass() + ".");

        this.reader = reader;
        this.writer = writer;
    }

    @Override
    public List<Shape3D> getShapes()
    {
        System.out.println("Get operation with writer: " + writer.getClass() + ".");

        return reader.getShapes();
    }

    @Override
    public boolean saveShapes(List<Shape3D> shape3DList)
    {
        System.out.println("Write operation with reader: " + reader.getClass() + ".");

        return writer.saveShapes(shape3DList);
    }

    public static ShapeReaderWriter getSql(){
        return new ShapeReaderWriter(new ShapeReaderMySql(), new ShapeWriterMySql());
    }

    public static ShapeReaderWriter getText(){
        return new ShapeReaderWriter(new ShapeReaderJson(), new ShapeWriterJson());
    }

    public static ShapeReaderWriter getObject()
    {
        return new ShapeReaderWriter(new ShapeReaderObject(), new ShapeWriterObject());
    }
}
