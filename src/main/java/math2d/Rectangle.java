package math2d;

import io.PropertyValueGet;
import math3d.Shape;

public class Rectangle extends Shape {
    private double width;
    private double length;

    // TODO Rectangle normally is length, width not vice versa. Discussion point. This means also new toString in unit test
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @PropertyValueGet(propertyName = "width")
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @PropertyValueGet(propertyName = "length")
    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", length=" + length + '}';
    }
}
