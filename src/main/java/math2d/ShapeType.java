package math2d;

public enum ShapeType {
    CIRCLE,
    RECTANGLE,
    TRIANGLE
}
