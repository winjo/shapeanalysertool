package math2d;

import io.PropertyValueGet;
import math3d.Shape;

public class Triangle extends Shape {
    protected double edge;

    public Triangle(double edge) {
        this.edge = edge;
    }

    @PropertyValueGet(propertyName = "edge")
    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public String toString() {
        return "Triangle{" + "edge=" + edge + '}';
    }
}
