import io.*;
import data.Shape3DStorageManager;
import math3d.*;

import java.util.Arrays;
import java.util.List;

/**
 * This is just our console mode sandbox.
 * Nothing special over here :)
 */
public class MainConsole {

    public static void main(String[] args) {
        Shape3DStorageManager shape3DStorageManager = new Shape3DStorageManager();

        List<Shape3D> shape3DList = Arrays.asList(
                new Cone(3.0, 5.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cylinder(2.0, 2.0),
                new OctaHedron(3.0),
                new Rhombicosidodecahedron(4.0),
                new Sphere(2.0)
        );

        shape3DStorageManager.setShape3DList(shape3DList);

        ShapeWriter shapeWriter = new ShapeWriterJson();
        shape3DStorageManager.saveShapes(shapeWriter);

        ShapeReader shapeReader = new ShapeReaderJson();
        shape3DStorageManager.setShape3DList(shapeReader);
    }
}
