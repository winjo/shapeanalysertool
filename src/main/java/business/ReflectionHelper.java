package business;

import java.lang.reflect.Method;

public final class ReflectionHelper
{
    public static <TClass> Method getMethod(TClass instance, String methodName) throws NoSuchMethodException
    {
        Class<?> clazz = instance.getClass();

        for (Method method : clazz.getMethods())
        {
            if (method.getName().equals(methodName)){
                return method;
            }
        }

        throw new NoSuchMethodException("Method '" + methodName + "' was not found.");
    }
}
