package business;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetterSetter<TValue>
{
    public GetterSetter(String propertyName, Method getter, Method setter)
    {
        this.propertyName = propertyName;
        this.getter = getter;
        this.setter = setter;
    }

    private Method getter;

    private Method setter;

    private String propertyName;

    public String getPropertyName()
    {
        return propertyName;
    }

    public <TInstanceType> void set(TInstanceType instance, TValue value)
    {
        try
        {
            setter.invoke(instance, value);
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        } catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    public <TInstanceType> TValue get(TInstanceType instance)
    {
        try
        {
            return (TValue) getter.invoke(instance);
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        } catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
