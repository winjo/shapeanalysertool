import data.Shape3DStorageManager;
import io.ShapeReaderMySql;
import io.ShapeReaderWriter;
import io.ShapeWriterMySql;
import view.Home;

import javax.swing.*;

/**
 * The main class is meant for the application to start up and do init stuff.
 */
public class Main {

    public static void main(String[] args) {

        Shape3DStorageManager shapeStorage = new Shape3DStorageManager();
        ShapeReaderWriter readerWriter = new ShapeReaderWriter(new ShapeReaderMySql(), new ShapeWriterMySql());
        readerWriter.getShapes().forEach(x -> shapeStorage.addShape(x));
        SwingUtilities.invokeLater(new Home(shapeStorage, readerWriter));
    }

}
