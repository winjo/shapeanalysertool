package view;

import data.Shape3DStorageManager;
import math3d.Shape3D;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProductButtonListener implements ActionListener {

    private Shape3DStorageManager shapeStorage;
    private JList storedShapeSummary;
    private JLabel totalVolumeLabel;
    private Shape3D shape;

    public ProductButtonListener(Shape3DStorageManager shapeStorage, JList storedShapeSummary, JLabel totalVolumeLabel, Shape3D shape) {
        this.shapeStorage = shapeStorage;
        this.storedShapeSummary = storedShapeSummary;
        this.totalVolumeLabel = totalVolumeLabel;
        this.shape = shape;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        shapeStorage.addShape(shape);
//        HashMap<Shape3D, Integer> shapeCount = shapeStorage.getShapes();
        String text = shape + "\n";

        DefaultListModel<String> shapeList = new DefaultListModel<>();
        shapeList.addElement(shape.toString());
        JList<String> list = new JList<>(shapeList);
        double totalVolume = shapeStorage.calculateTotalVolume();
        totalVolumeLabel.setText("Totaal: " + totalVolume);

    }
}
