package view;

import business.GetterSetter;
import data.Shape3DStorageManager;
import io.ShapeReaderWriter;
import math3d.Shape3D;
import view.interfaces.IShapeDropDownItem;
import view.models.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Home implements Runnable
{
    private ShapeReaderWriter shapeReaderWriter;
    private Shape3DStorageManager storageManager;

    private JComboBox shapeSelector;
    private JList shapeStorageDisplay;
    private JFrame frame;
    private JPanel shapePropertiesForm;
    private JPanel left;
    private JPanel right;
    private JLabel volumeLabel;

    public Home(Shape3DStorageManager storageManager, ShapeReaderWriter shapeReaderWriter)
    {
        this.storageManager = storageManager;
        this.shapeReaderWriter = shapeReaderWriter;

        setLeft();
        setRight();
        setMainFrame();
        setShapeChoices();
        setUserDefinedShapeListDisplay();
        setShapePropertiesForm(null);
        setExportBtn();
        setChangeDataSourceBtn();
        setClearBtn();

        volumeLabel = new JLabel();
        frame.add(volumeLabel);
        setTotalVolumeLabel();
    }

    private void setTotalVolumeLabel()
    {
        volumeLabel.setText("Total volume: " + storageManager.calculateTotalVolume());
    }

    private void setClearBtn()
    {
        JButton delbtn = new JButton("Clear list");
        delbtn.addActionListener(x -> storageManager.deleteAllShapes());
        right.add(delbtn);
    }

    private void setExportBtn()
    {
        JButton exportBtn = new JButton("Export");
        exportBtn.addActionListener(x -> storageManager.saveShapes(shapeReaderWriter));
        right.add(exportBtn);
    }

    private void setChangeDataSourceBtn()
    {
        JButton changedsBtn = new JButton("Change data-source");
        changedsBtn.addActionListener(x -> changeDataSource());
        right.add(changedsBtn);
    }

    private void changeDataSource()
    {

        ChangeStorageMethodDialogue dialogue = new ChangeStorageMethodDialogue();
        dialogue.setVisible(true);
        dialogue.addActionListener(x ->
        {
            setShapeReaderWriter(x);
            storageManager.deleteAllShapes();
            try
            {
                java.util.List<Shape3D> shapes = getShapes();

                if (shapes != null)
                {
                    storageManager.setShape3DList(shapes);
                }
                else{
                    updateUserDefinedShapeDisplay();
                }

            } catch (Exception e)
            {
                JOptionPane.showMessageDialog(frame, "Something has gone wrong. Please try again.");
                e.printStackTrace();
            }
        });

        dialogue.addActionListener(x -> dialogue.dispose());
    }

    private List<Shape3D> getShapes()
    {
        return shapeReaderWriter.getShapes();
    }

    private void setShapeReaderWriter(ShapeReaderWriter srw)
    {
        this.shapeReaderWriter = srw;
    }

    private void setRight()
    {
        right = new JPanel();
        right.setLayout(new FlowLayout());
    }

    private void setLeft()
    {
        left = new JPanel();
        left.setLayout(new FlowLayout());
    }

    private void setMainFrame()
    {
        frame = new JFrame("Home");
        frame.setSize(1000, 1000);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridLayout gl = new GridLayout(1, 2);
        frame.add(left);
        frame.add(right);
        frame.setLayout(gl);
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

    }

    private void setShapePropertiesForm(IShapeDropDownItem shape)
    {
        if (shapePropertiesForm == null)
        {
            shapePropertiesForm = new JPanel();
            shapePropertiesForm.setLayout(new GridLayout(5, 1));
            left.add(shapePropertiesForm);
        }
        else
        {
            shapePropertiesForm.removeAll();
        }

        if (shape != null)
        {
            try
            {

                Shape3D newShapeInstance = (Shape3D) (shape).createInstance();

                for (GetterSetter property : shape.getProperties())
                {
                    JLabel label = new JLabel(property.getPropertyName());
                    shapePropertiesForm.add(label);

                    JSlider slider = new JSlider();
                    property.set(newShapeInstance, (double) slider.getValue());

                    slider.addChangeListener(a -> property.set(newShapeInstance, (double) slider.getValue()));

                    shapePropertiesForm.add(slider);
                }

                JButton submitbtn = new JButton("Submit");
                submitbtn.addActionListener(e ->
                {
                    storageManager.addShape(newShapeInstance);
                    shapeSelector.setSelectedIndex(0);
                });
                shapePropertiesForm.add(submitbtn);
            } catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            }
        }

        shapePropertiesForm.repaint();
        shapePropertiesForm.revalidate();
    }

    private void setUserDefinedShapeListDisplay()
    {
        if (shapeStorageDisplay == null)
        {
            shapeStorageDisplay = new JList();
            right.add(shapeStorageDisplay);
            storageManager.addActionListener(x ->
            {
                updateUserDefinedShapeDisplay();
                setTotalVolumeLabel();
            });
            updateUserDefinedShapeDisplay();
        }
    }

    private void updateUserDefinedShapeDisplay()
    {
        shapeStorageDisplay.setListData(storageManager.getShape3DList().toArray());
        shapeStorageDisplay.repaint();
        shapeStorageDisplay.revalidate();
    }

    private void setShapeChoices()
    {
        shapeSelector = new JComboBox();
        shapeSelector.addItem(null);
        shapeSelector.addItem(new ConeDropDownItem());
        shapeSelector.addItem(new CuboidDropDownItem());
        shapeSelector.addItem(new CylinderDropDownItem());
        shapeSelector.addItem(new OctaHedronDropDownItem());
        shapeSelector.addItem(new RhombicosidodecahedronDropDownItem());
        shapeSelector.addItem(new SphereDropDownItem());

        shapeSelector.addActionListener(e -> onShapeChoiceMade(e));

        left.add(shapeSelector);
    }

    private void onShapeChoiceMade(ActionEvent e)
    {
        setShapePropertiesForm((IShapeDropDownItem) shapeSelector.getSelectedItem());
    }

    @Override
    public void run()
    {
        frame.pack();
        frame.setVisible(true);
    }
}
