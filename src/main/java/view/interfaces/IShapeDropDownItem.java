package view.interfaces;

import business.GetterSetter;
import math3d.Shape;

import java.util.ArrayList;
import java.util.HashSet;

public interface IShapeDropDownItem
{
    ArrayList<GetterSetter> getProperties() throws NoSuchMethodException;

    public Object createInstance();
}
