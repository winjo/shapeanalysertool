package view;

import io.ShapeReaderWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class ChangeStorageMethodDialogue extends JDialog
{
    private ArrayList<Consumer<ShapeReaderWriter>> listenerList;

    public ChangeStorageMethodDialogue()
    {
        listenerList = new ArrayList<>();
        addButtons();
    }


    private void addButtons()
    {
        setSize(600, 200);
        setLayout(new GridLayout());

        JButton btnSql = new JButton("MySql");
        btnSql.addActionListener(x -> setShapeReaderWriter(ShapeReaderWriter.getSql()));
        add(btnSql);

        JButton btnFile = new JButton("Text");
        btnFile.addActionListener(x -> setShapeReaderWriter(ShapeReaderWriter.getText()));
        add(btnFile);

        JButton btnObj = new JButton("Object");
        btnObj.addActionListener(x -> setShapeReaderWriter(ShapeReaderWriter.getObject()));
        add(btnObj);
    }

    private void setShapeReaderWriter(ShapeReaderWriter srw){
        listenerList.forEach(x ->
        {
            x.accept(srw);
        });
    }

    public void addActionListener(Consumer<ShapeReaderWriter> func)
    {
        listenerList.add(func);
    }
}
