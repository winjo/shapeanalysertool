package view;

import data.Shape3DStorageManager;
import math3d.Shape3D;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ShapeAnalyzeToolUIOLD implements Runnable {
    private JFrame frame;
    private Shape3DStorageManager shapeStorage;

    public ShapeAnalyzeToolUIOLD(Shape3DStorageManager shapeStorage) {
        this.shapeStorage = shapeStorage;
    }


    @Override
    public void run() {
        frame = new JFrame("VAT");
        frame.setPreferredSize(new Dimension(500, 300));

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.pack();
        frame.setVisible(true);
    }
}

