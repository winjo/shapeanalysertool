package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.Cuboid;
import math3d.Cylinder;
import view.interfaces.IShapeDropDownItem;

import java.util.ArrayList;

public class CylinderDropDownItem extends Cylinder implements IShapeDropDownItem
{
    public CylinderDropDownItem()
    {
        super(0, 0);
    }

    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Radius", ReflectionHelper.getMethod(this, "getRadius"), ReflectionHelper.getMethod(this, "setRadius")));
        r.add(new GetterSetter("Height", ReflectionHelper.getMethod(this, "getHeight"), ReflectionHelper.getMethod(this, "setHeight")));

        return r;
    }

    @Override
    public Cylinder createInstance()
    {
        return new Cylinder(0, 0);
    }

    @Override
    public String toString()
    {
        return "Cylinder";
    }
}
