package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.Cylinder;
import view.interfaces.IShapeDropDownItem;
import math3d.OctaHedron;

import java.util.ArrayList;

public class OctaHedronDropDownItem extends OctaHedron implements IShapeDropDownItem
{
    public OctaHedronDropDownItem()
    {
        super(0);
    }

    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Edge", ReflectionHelper.getMethod(this, "getEdge"), ReflectionHelper.getMethod(this, "setEdge")));

        return r;
    }

    @Override
    public OctaHedron createInstance()
    {
        return new OctaHedron(0);
    }

    @Override
    public String toString()
    {
        return "OctaHedron";
    }
}
