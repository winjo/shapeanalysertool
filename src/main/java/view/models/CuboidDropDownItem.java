package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.Cone;
import math3d.Cuboid;
import view.interfaces.IShapeDropDownItem;

import java.util.ArrayList;

public class CuboidDropDownItem extends Cuboid implements IShapeDropDownItem
{
    public CuboidDropDownItem()
    {
        super(0, 0, 0);
    }

    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Width", ReflectionHelper.getMethod(this, "getWidth"), ReflectionHelper.getMethod(this, "setWidth")));
        r.add(new GetterSetter("Length", ReflectionHelper.getMethod(this, "getLength"), ReflectionHelper.getMethod(this, "setLength")));
        r.add(new GetterSetter("Height", ReflectionHelper.getMethod(this, "getHeight"), ReflectionHelper.getMethod(this, "setHeight")));

        return r;
    }

    @Override
    public Cuboid createInstance()
    {
        return new Cuboid(0, 0,0);
    }

    @Override
    public String toString()
    {
        return "Cuboid";
    }
}
