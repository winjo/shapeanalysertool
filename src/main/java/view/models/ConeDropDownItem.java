package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.Cone;
import view.interfaces.IShapeDropDownItem;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class ConeDropDownItem extends Cone implements IShapeDropDownItem
{
    public ConeDropDownItem()
    {
        super(0, 0);
    }


    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Height", ReflectionHelper.getMethod(this, "getHeight"), ReflectionHelper.getMethod(this, "setHeight")));
        r.add(new GetterSetter("Radius", ReflectionHelper.getMethod(this, "getRadius"), ReflectionHelper.getMethod(this, "setRadius")));

        return r;
    }

    @Override
    public Cone createInstance()
    {
        return new Cone(0, 0);
    }

    @Override
    public String toString()
    {
        return "Cone";
    }
}
