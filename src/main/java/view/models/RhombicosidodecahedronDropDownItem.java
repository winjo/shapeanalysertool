package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.OctaHedron;
import math3d.Rhombicosidodecahedron;
import view.interfaces.IShapeDropDownItem;

import java.util.ArrayList;

public class RhombicosidodecahedronDropDownItem extends Rhombicosidodecahedron implements IShapeDropDownItem
{
    public RhombicosidodecahedronDropDownItem()
    {
        super(0);
    }

    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Edge", ReflectionHelper.getMethod(this, "getEdge"), ReflectionHelper.getMethod(this, "setEdge")));

        return r;
    }

    @Override
    public OctaHedron createInstance()
    {
        return new OctaHedron(0);
    }

    @Override
    public String toString()
    {
        return "Rhombicosidodecahedron";
    }
}
