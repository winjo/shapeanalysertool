package view.models;

import business.GetterSetter;
import business.ReflectionHelper;
import math3d.OctaHedron;
import math3d.Sphere;
import view.interfaces.IShapeDropDownItem;

import java.util.ArrayList;

public class SphereDropDownItem extends Sphere implements IShapeDropDownItem
{
    public SphereDropDownItem()
    {
        super(0);
    }

    @Override
    public ArrayList<GetterSetter> getProperties() throws NoSuchMethodException
    {
        ArrayList<GetterSetter> r = new ArrayList<>();

        r.add(new GetterSetter("Radius", ReflectionHelper.getMethod(this, "getRadius"), ReflectionHelper.getMethod(this, "setRadius")));

        return r;
    }

    @Override
    public Sphere createInstance()
    {
        return new Sphere(0);
    }

    @Override
    public String toString()
    {
        return "Sphere";
    }
}
