package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class OctaHedronTest {

    private OctaHedron octaHedron;
    private final double initEdge = 4.0;

    @BeforeEach
    void setUp() {
        octaHedron = new OctaHedron(this.initEdge);
    }

    @Test
    @DisplayName("getEdge functionality")
    void getEdge() {
        // Arrange - test preparation, create test object, initialise etc.
        double edge;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        edge = this.octaHedron.getEdge();

        // Assert - assertions on the expected and actual value
        assertEquals(edge, this.initEdge, "OctaHedron edge should be the sames as init.");
    }

    @Test
    @DisplayName("setEdge functionality")
    void setEdge() {
        // Arrange - test preparation, create test object, initialise etc.
        double edge = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.octaHedron.setEdge(edge);

        // Assert - assertions on the expected and actual value
        assertEquals(this.octaHedron.getEdge(), edge, "OctaHedron edge should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.octaHedron.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "OctaHedron{" + "edge=" + this.initEdge + '}', "OctaHedron toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.octaHedron.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.octaHedron.setUuid(uuid);
        getUuid = this.octaHedron.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("calculateVolume functionality")
    void calculateVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double volume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        volume = this.octaHedron.calculateVolume();

        // Assert - assertions on the expected and actual value
        assertTrue(this.initEdge > 0, "Init edge must be higher than 0 for this test");
        assertTrue(volume > 0, "Volume calculation failed. Is the formula filled in?");
        assertEquals(volume, (this.initEdge * this.initEdge * this.initEdge) * (Math.sqrt(2) / 3),
                "Volume calculation result different. Is the formula changed?");
    }
}