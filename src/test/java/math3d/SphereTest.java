package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class SphereTest {

    private Sphere sphere;
    private final double initRadius = 4.0;

    @BeforeEach
    void setUp() {
        sphere = new Sphere(this.initRadius);
    }

    @Test
    @DisplayName("getRadius functionality")
    void getRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        radius = this.sphere.getRadius();

        // Assert - assertions on the expected and actual value
        assertEquals(radius, this.initRadius, "Sphere radius should be the sames as init.");
    }

    @Test
    @DisplayName("setRadius functionality")
    void setRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.sphere.setRadius(radius);

        // Assert - assertions on the expected and actual value
        assertEquals(this.sphere.getRadius(), radius, "Sphere radius should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.sphere.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "Sphere{" + "radius=" + this.initRadius + '}', "Sphere toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.sphere.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.sphere.setUuid(uuid);
        getUuid = this.sphere.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("calculateVolume functionality")
    void calculateVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double volume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        volume = this.sphere.calculateVolume();

        // Assert - assertions on the expected and actual value
        assertTrue(this.initRadius > 0, "Init radius must be higher than 0 for this test");
        assertTrue(volume > 0, "Volume calculation failed. Is the formula filled in?");
        assertEquals(volume, 3d / 4 * Math.PI * this.sphere.getRadius() * this.sphere.getRadius() * this.sphere.getRadius(),
                "Volume calculation result different. Is the formula changed?");
    }
}