package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class CylinderTest {

    private Cylinder cylinder;
    private final double initRadius = 4.0;
    private final double initHeight = 2.0;

    @BeforeEach
    void setUp() {
        this.cylinder = new Cylinder(this.initRadius, this.initHeight);
    }

    @Test
    @DisplayName("getRadius functionality")
    void getRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        radius = this.cylinder.getRadius();

        // Assert - assertions on the expected and actual value
        assertEquals(radius, this.initRadius, "Cylinder radius should be the sames as init.");
    }

    @Test
    @DisplayName("setRadius functionality")
    void setRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cylinder.setRadius(radius);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cylinder.getRadius(), radius, "Cylinder radius should be the sames as set.");
    }

    @Test
    @DisplayName("getHeight functionality")
    void getHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        height = this.cylinder.getHeight();

        // Assert - assertions on the expected and actual value
        assertEquals(height, this.initHeight, "Cylinder height should be the sames as init.");
    }

    @Test
    @DisplayName("setHeight functionality")
    void setHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height = 5.0;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cylinder.setHeight(height);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cylinder.getHeight(), height, "Cylinder height should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cylinder.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "Cylinder{" + "radius=" + this.cylinder.getRadius() + ", height=" + this.cylinder.getHeight() + '}',
                "Cylinder toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cylinder.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cylinder.setUuid(uuid);
        getUuid = this.cylinder.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("calculateVolume functionality")
    void calculateVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double volume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        volume = this.cylinder.calculateVolume();

        // Assert - assertions on the expected and actual value
        assertTrue(this.initRadius > 0, "Init radius must be higher than 0 for this test");
        assertTrue(this.initHeight > 0, "Init height must be higher than 0 for this test");
        assertTrue(volume > 0, "Volume calculation failed. Is the formula filled in?");
        assertEquals(volume, (22 * this.cylinder.getRadius() * this.cylinder.getRadius() * this.cylinder.getHeight()) / 7,
                "Volume calculation result different. Is the formula changed?");
    }
}