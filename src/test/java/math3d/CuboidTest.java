package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class CuboidTest {

    private Cuboid cuboid;
    private final double initWidth = 4.0;
    private final double initLength = 3.0;
    private final double initHeight = 2.0;

    @BeforeEach
    void setUp() {
        this.cuboid = new Cuboid(this.initWidth, this.initLength, this.initHeight);
    }

    @Test
    @DisplayName("getWidth functionality")
    void getWidth() {
        // Arrange - test preparation, create test object, initialise etc.
        double width;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        width = this.cuboid.getWidth();

        // Assert - assertions on the expected and actual value
        assertEquals(width, this.initWidth, "Cuboid width should be the sames as init.");
    }

    @Test
    @DisplayName("setWidth functionality")
    void setWidth() {
        // Arrange - test preparation, create test object, initialise etc.
        double width = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cuboid.setWidth(width);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cuboid.getWidth(), width, "Cuboid width should be the sames as set.");
    }

    @Test
    @DisplayName("getLength functionality")
    void getLength() {
        // Arrange - test preparation, create test object, initialise etc.
        double length;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        length = this.cuboid.getLength();

        // Assert - assertions on the expected and actual value
        assertEquals(length, this.initLength, "Cuboid length should be the sames as init.");
    }

    @Test
    @DisplayName("setLength functionality")
    void setLength() {
        // Arrange - test preparation, create test object, initialise etc.
        double length = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cuboid.setLength(length);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cuboid.getLength(), length, "Cuboid length should be the sames as set.");
    }

    @Test
    @DisplayName("getHeight functionality")
    void getHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        height = this.cuboid.getHeight();

        // Assert - assertions on the expected and actual value
        assertEquals(height, this.initHeight, "Cuboid height should be the sames as init.");
    }

    @Test
    @DisplayName("setHeight functionality")
    void setHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height = 5.0;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cuboid.setHeight(height);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cuboid.getHeight(), height, "Cuboid height should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cuboid.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "Cuboid{" + "width=" + this.cuboid.getWidth() + ", length=" + this.cuboid.getLength()
                        + ", height=" + this.cuboid.getHeight() + '}',
                "Cuboid toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cuboid.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cuboid.setUuid(uuid);
        getUuid = this.cuboid.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("calculateVolume functionality")
    void calculateVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double volume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        volume = this.cuboid.calculateVolume();

        // Assert - assertions on the expected and actual value
        assertTrue(this.initWidth > 0, "Init width must be higher than 0 for this test");
        assertTrue(this.initLength > 0, "Init length must be higher than 0 for this test");
        assertTrue(this.initHeight > 0, "Init height must be higher than 0 for this test");
        assertTrue(volume > 0, "Volume calculation failed. Is the formula filled in?");
        assertEquals(volume, this.cuboid.getWidth() * this.cuboid.getLength() * this.cuboid.getHeight(),
                "Volume calculation result different. Is the formula changed?");
    }
}