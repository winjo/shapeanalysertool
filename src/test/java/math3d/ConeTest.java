package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ConeTest {

    private Cone cone;
    private final double initRadius = 4.0;
    private final double initHeight = 2.0;

    @BeforeEach
    void setUp() {
        this.cone = new Cone(this.initRadius, this.initHeight);
    }

    @Test
    @DisplayName("getRadius functionality")
    void getRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        radius = this.cone.getRadius();

        // Assert - assertions on the expected and actual value
        assertEquals(radius, this.initRadius, "Cone radius should be the sames as init.");
    }

    @Test
    @DisplayName("setRadius functionality")
    void setRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cone.setRadius(radius);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cone.getRadius(), radius, "Cone radius should be the sames as set.");
    }

    @Test
    @DisplayName("getHeight functionality")
    void getHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        height = this.cone.getHeight();

        // Assert - assertions on the expected and actual value
        assertEquals(height, this.initHeight, "Cone height should be the sames as init.");
    }

    @Test
    @DisplayName("setHeight functionality")
    void setHeight() {
        // Arrange - test preparation, create test object, initialise etc.
        double height = 5.0;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cone.setHeight(height);

        // Assert - assertions on the expected and actual value
        assertEquals(this.cone.getHeight(), height, "Cone height should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cone.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "Cone{" + "radius=" + this.cone.getRadius() + ", height=" + this.cone.getHeight() + '}',
                "Cone toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.cone.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.cone.setUuid(uuid);
        getUuid = this.cone.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("calculateVolume functionality")
    void calculateVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double volume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        volume = this.cone.calculateVolume();

        // Assert - assertions on the expected and actual value
        assertTrue(this.initRadius > 0, "Init radius must be higher than 0 for this test");
        assertTrue(this.initHeight > 0, "Init height must be higher than 0 for this test");
        assertTrue(volume > 0, "Volume calculation failed. Is the formula filled in?");
        assertEquals(volume, (1.0 / 3) * Math.PI * this.cone.getRadius() * this.cone.getRadius() * this.cone.getHeight(),
                "Volume calculation result different. Is the formula changed?");
    }
}