package math3d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * We're making use of a dummyShape here to test the Shape class since Shape is an abstract class.
 */
class ShapeTest {

    private DummyShape dummyShape;

    @BeforeEach
    void setUp() {
        this.dummyShape = new DummyShape();
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.dummyShape.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.dummyShape.setUuid(uuid);
        getUuid = this.dummyShape.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }
}

/**
 * Make use of dummyShape to test the abstract Shape class
 */
class DummyShape extends Shape {
    DummyShape() {
    }
}