package data;

import io.ShapeReader;
import io.ShapeReaderJson;
import io.ShapeWriter;
import io.ShapeWriterJson;
import math3d.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Shape3DStorageManagerTest {

    private Shape3DStorageManager shape3DStorageManager;
    private List<Shape3D> shape3DList;

    @BeforeEach
    void setUp() {
        // Initialise Shape3DStorageManager and add shapes for testing
        shape3DStorageManager = new Shape3DStorageManager();
        shape3DList = Arrays.asList(
                new Cone(3.0, 5.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cylinder(2.0, 2.0),
                new OctaHedron(3.0),
                new Rhombicosidodecahedron(4.0),
                new Sphere(2.0)
        );
    }

    @Test
    @DisplayName("addShape functionality")
    void addShape() {
        // Arrange - test preparation, create test object, initialise etc.
        int beforeListSize;
        int afterListSize;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        Shape3D shape3D = shape3DList.get(0); // Get the cone
        beforeListSize = shape3DStorageManager.getShape3DList().size(); // Before
        shape3DStorageManager.addShape(shape3D);
        afterListSize = shape3DStorageManager.getShape3DList().size(); // After

        // Assert - assertions on the expected and actual value
        assertEquals(afterListSize - beforeListSize, 1, "Size should be 1 since only one object has been added.");
    }

    @Test
    @DisplayName("addShapes functionality")
    void addShapes() {
        // Arrange - test preparation, create test object, initialise etc.
        int beforeListSize;
        int afterListSize;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        beforeListSize = shape3DStorageManager.getShape3DList().size(); // Before
        shape3DStorageManager.addShapes(shape3DList);
        afterListSize = shape3DStorageManager.getShape3DList().size(); // After

        // Assert - assertions on the expected and actual value
        assertNotEquals(beforeListSize, afterListSize, "There should be multiple shapes in the list");
    }

    @Test
    @DisplayName("deleteShape functionality")
    void deleteShape() {
        // Arrange - test preparation, create test object, initialise etc.
        int beforeListSize;
        int afterListSize;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DStorageManager.addShapes(shape3DList);
        beforeListSize = shape3DStorageManager.getShape3DList().size(); // Before
        shape3DStorageManager.deleteShape(0); // Delete the first shape
        afterListSize = shape3DStorageManager.getShape3DList().size(); // After

        // Assert - assertions on the expected and actual value
        assertEquals(beforeListSize - afterListSize, 1, "There should be less shapes in the list");
    }

    @Test
    @DisplayName("deleteShapes functionality")
    void deleteShapes() {
        // Arrange - test preparation, create test object, initialise etc.
        int beforeListSize;
        int afterListSize;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DStorageManager.addShapes(shape3DList);
        beforeListSize = shape3DStorageManager.getShape3DList().size(); // Before
        shape3DStorageManager.deleteShapes(shape3DList); // Deletes all the shapes given in the list
        afterListSize = shape3DStorageManager.getShape3DList().size(); // After

        // Assert - assertions on the expected and actual value
        assertEquals(afterListSize, 0, "There should be 0 shapes left in the list");
    }

    @Test
    @DisplayName("calculateTotalVolume functionality")
    void calculateTotalVolume() {
        // Arrange - test preparation, create test object, initialise etc.
        double beforeVolume;
        double afterVolume;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        beforeVolume = shape3DStorageManager.calculateTotalVolume(); // Before
        shape3DStorageManager.addShapes(shape3DList);
        afterVolume = shape3DStorageManager.calculateTotalVolume(); // After

        // Assert - assertions on the expected and actual value
        assertTrue(afterVolume > 0, "Volume has to be positive");
        assertTrue(shape3DList.size() > 0, "There has to be at least 1 shape3D object");
        assertNotEquals(beforeVolume, afterVolume, "Volume before cannot be the same");
    }

    @Test
    @DisplayName("getShape3DList functionality")
    void getShape3DList() {
        // Arrange - test preparation, create test object, initialise etc.
        List<Shape3D> shape3DList;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DStorageManager.addShapes(this.shape3DList);
        shape3DList = shape3DStorageManager.getShape3DList();

        // Assert - assertions on the expected and actual value
        assertTrue(shape3DList.size() > 0, "There has to be at least 1 shape3D object");
        assertEquals(shape3DList, this.shape3DList, "Shape 3D lists has to be the same");
    }

    @Test
    @DisplayName("setShape3DList functionality")
    void setShape3DList() {
        // Arrange - test preparation, create test object, initialise etc.
        List<Shape3D> shape3DList;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DStorageManager.setShape3DList(this.shape3DList);
        shape3DList = shape3DStorageManager.getShape3DList();

        // Assert - assertions on the expected and actual value
        assertTrue(shape3DList.size() > 0, "There has to be at least 1 shape3D object");
        assertEquals(shape3DList, this.shape3DList, "Shape 3D lists has to be the same");
    }

    @Test
    @DisplayName("saveShapes functionality")
    void saveShapes() {
        // Arrange - test preparation, create test object, initialise etc.
        List<Shape3D> shape3DList;
        ShapeReader shapeReader = new ShapeReaderJson();
        ShapeWriter shapeWriter = new ShapeWriterJson();

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DStorageManager.setShape3DList(this.shape3DList);
        shape3DStorageManager.saveShapes(shapeWriter);
        shape3DList = shapeReader.getShapes();
        shape3DStorageManager.setShape3DList(shape3DList);

        // Assert - assertions on the expected and actual value
        assertTrue(shape3DList != null && shape3DList.size() > 0, "There has to be at least 1 shape3D object");
        assertEquals(this.shape3DList.toString(), shape3DList.toString(), "Shape 3D lists has to be the same");
    }
}