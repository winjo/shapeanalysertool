package settings;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class appSettingsTest {

    @Test
    @DisplayName("getMysql_host functionality")
    void getMySql_host() {
        // Arrange - test preparation, create test object, initialise etc.
        String mySqlHost;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        mySqlHost = appSettings.getMySql_host().trim();

        // Assert - assertions on the expected and actual value
        assertNotEquals(mySqlHost,"", "MySQL connection host cannot be empty.");

    }

    @Test
    @DisplayName("getMysql_user functionality")
    void getMySql_user() {
        // Arrange - test preparation, create test object, initialise etc.
        String mySqlUser;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        mySqlUser = appSettings.getMySql_user().trim();

        // Assert - assertions on the expected and actual value
        assertNotEquals(mySqlUser,"", "MySQL user cannot be empty.");
    }

    @Test
    @DisplayName("getMysql_password functionality")
    void getMySql_password() {
        // Arrange - test preparation, create test object, initialise etc.
        String mySqlPassword;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        mySqlPassword = settings.appSettings.getMySql_password().trim();

        // Assert - assertions on the expected and actual value
        assertNotEquals(mySqlPassword,"", "MySQL connection password cannot be empty.");
    }
}