package io;

import math3d.Shape3D;
import math3d.Sphere;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShapeReaderJsonTest {

    @Test
    void getShapes() {
        // Arrange - test preparation, create test object, initialise etc.
        List<Shape3D> shape3DList;
        ShapeReader shapeReader = new ShapeReaderJson();
        boolean isSuccess;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        shape3DList = shapeReader.getShapes();
        isSuccess = shape3DList != null;
        // Assert - assertions on the expected and actual value
        assertTrue(isSuccess, "Getting shapes failed using the reader. Is the file empty/corrupt?");
    }
}