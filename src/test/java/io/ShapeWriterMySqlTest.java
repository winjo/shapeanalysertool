package io;

import math3d.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The test is not fully automatic since we are also trying to test the saveDialog.
 * Another saveShapes with a manual path could be added to make it more automatic and skip the saveDialog.
 * Since we lack time and it's not that important we aren't going to implement that right now.
 */
class ShapeWriterMySqlTest {

    private List<Shape3D> shape3DList;

    @BeforeEach
    void setUp() {
        // Initialise Shape3DStorageManager and add shapes for testing
        shape3DList = Arrays.asList(
                new Cone(3.0, 5.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cuboid(2.0, 3.0, 4.0),
                new Cylinder(2.0, 2.0),
                new OctaHedron(3.0),
                new Rhombicosidodecahedron(4.0),
                new Sphere(2.0)
        );
    }

    @Test
    @DisplayName("saveShapes functionality")
    void saveShapes() {
        // Arrange - test preparation, create test object, initialise etc.
        ShapeWriter shapeWriter = new ShapeWriterMySql();
        boolean isSuccess;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        isSuccess = shapeWriter.saveShapes(shape3DList);

        // Assert - assertions on the expected and actual value
        assertTrue(isSuccess, "Save shapes failed using the writer");
    }
}