package math2d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.w3c.dom.css.Rect;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    private Rectangle rectangle;
    private final double initLength = 4.0;
    private final double initWidth = 3.0;

    @BeforeEach
    void setUp() {
        this.rectangle = new Rectangle(this.initWidth, this.initLength);
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.rectangle.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.rectangle.setUuid(uuid);
        getUuid = this.rectangle.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("getWidth functionality")
    void getWidth() {
        // Arrange - test preparation, create test object, initialise etc.
        double width;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        width = this.rectangle.getWidth();

        // Assert - assertions on the expected and actual value
        assertEquals(width, this.initWidth, "Rectangle width should be the sames as init.");
    }

    @Test
    @DisplayName("setWidth functionality")
    void setWidth() {
        // Arrange - test preparation, create test object, initialise etc.
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.rectangle.setWidth(this.initWidth);

        // Assert - assertions on the expected and actual value
        assertEquals(this.rectangle.getWidth(), this.initWidth, "Rectangle width should be the sames as set.");
    }

    @Test
    @DisplayName("getLength functionality")
    void getLength() {
        // Arrange - test preparation, create test object, initialise etc.
        double length;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        length = this.rectangle.getLength();

        // Assert - assertions on the expected and actual value
        assertEquals(length, this.initLength, "Rectangle length should be the sames as init.");
    }

    @Test
    @DisplayName("setLength functionality")
    void setLength() {
        // Arrange - test preparation, create test object, initialise etc.
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.rectangle.setLength(this.initLength);

        // Assert - assertions on the expected and actual value
        assertEquals(this.rectangle.getLength(), this.initLength, "Rectangle length should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.rectangle.toString();
        // Assert - assertions on the expected and actual value
        assertEquals(output, "Rectangle{" + "width=" + this.initWidth + ", length=" + this.initLength + '}', "Circle toString has been changed.");
    }
}