package math2d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    private Triangle triangle;
    private final double initEdge = 4.0;

    @BeforeEach
    void setUp() {
        triangle = new Triangle(this.initEdge);
    }

    @Test
    @DisplayName("getEdge functionality")
    void getEdge() {
        // Arrange - test preparation, create test object, initialise etc.
        double edge;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        edge = this.triangle.getEdge();

        // Assert - assertions on the expected and actual value
        assertEquals(edge, this.initEdge, "Triangle edge should be the sames as init.");
    }

    @Test
    @DisplayName("setEdge functionality")
    void setEdge() {
        // Arrange - test preparation, create test object, initialise etc.
        double edge = 5.0;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.triangle.setEdge(edge);

        // Assert - assertions on the expected and actual value
        assertEquals(this.triangle.getEdge(), edge, "Triangle edge should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.triangle.toString();

        // Assert - assertions on the expected and actual value
        assertEquals(output, "Triangle{" + "edge=" + this.initEdge + '}', "Triangle toString has been changed.");
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.triangle.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.triangle.setUuid(uuid);
        getUuid = this.triangle.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }
}