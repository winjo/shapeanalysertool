package math2d;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    private Circle circle;
    private final double initRadius = 4.0;

    @BeforeEach
    void setUp() {
        this.circle = new Circle(this.initRadius);
    }

    @Test
    @DisplayName("getUuid functionality")
    void getUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.circle.getUuid().toString();

        // Assert - assertions on the expected and actual value
        assertFalse(output.trim().isEmpty(), "UUID cannot be empty.");
    }

    @Test
    @DisplayName("setUuid functionality")
    void setUuid() {
        // Arrange - test preparation, create test object, initialise etc.
        UUID uuid = UUID.randomUUID();
        UUID getUuid;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.circle.setUuid(uuid);
        getUuid = this.circle.getUuid();

        // Assert - assertions on the expected and actual value
        assertEquals(uuid, getUuid, "UUID must be equal.");
    }

    @Test
    @DisplayName("getRadius functionality")
    void getRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        double radius;

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        radius = this.circle.getRadius();

        // Assert - assertions on the expected and actual value
        assertEquals(radius, this.initRadius, "Circle radius should be the sames as init.");
    }

    @Test
    @DisplayName("setRadius functionality")
    void setRadius() {
        // Arrange - test preparation, create test object, initialise etc.
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        this.circle.setRadius(5.0);

        // Assert - assertions on the expected and actual value
        assertEquals(this.circle.getRadius(), 5.0, "Circle radius should be the sames as set.");
    }

    @Test
    @DisplayName("toString functionality")
    void testToString() {
        // Arrange - test preparation, create test object, initialise etc.
        String output;
        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method
        output = this.circle.toString();
        // Assert - assertions on the expected and actual value
        assertEquals(output, "Circle{" + "radius=" + this.circle.getRadius() + '}', "Circle toString has been changed.");
    }
}