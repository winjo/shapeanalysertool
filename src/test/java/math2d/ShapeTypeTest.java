package math2d;

import math3d.ShapeType3D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * You might wonder why you should test enums?
 * In this case it's not really necessary, but in bigger applications it can be very necessary since they are required.
 * This is especially the case with Test Driven Development.
 * There are no methods in the enum but still I think that it's good to do anyway.
 */

class ShapeTypeTest {
    private enum ShapeType3DCopy {
        CONE,
        CUBOID,
        CYLINDER,
        OCTAHEDRON,
        RHOMBICOSIDODECAHEDRON,
        SPHERE
    }

    @Test
    @DisplayName("enumsEmpty functionality")
    void enumsEmpty() {
        // Arrange - test preparation, create test object, initialise etc.
        ShapeType3D[] shapeType3DValues = ShapeType3D.values();

        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method

        // Assert - assertions on the expected and actual value
        assertNotEquals(shapeType3DValues.length, 0, "The ShapeType3D enum contains 0 Shape3D types.");
    }

    @Test
    @DisplayName("enumsChanged functionality")
    void enumsChanged() {
        // Arrange - test preparation, create test object, initialise etc.
        List<String> shapeType3DValues = new ArrayList<>();
        List<String> shapeType3DCopyValues = new ArrayList<>();

        for (ShapeType3D shapeType3D : ShapeType3D.values()) {
            shapeType3DValues.add(shapeType3D.name());
        }

        for (ShapeType3DCopy shapeType3D : ShapeType3DCopy.values()) {
            shapeType3DCopyValues.add(shapeType3D.name());
        }

        boolean isSame = shapeType3DValues.equals(shapeType3DCopyValues);


        // Empty, done in @BeforeEach

        // Act - actual calls of the tested method

        // Assert - assertions on the expected and actual value
        assertTrue(isSame, "Shape3D types are used by other functions thus required.");
    }
}