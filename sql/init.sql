create database ShapeAnalyser;

use ShapeAnalyser;

create table Shape
(
    Uuid char(36) unique,
    ShapeType varchar(32) not null,
    primary key (Uuid)
);

create table ShapeProperty
(
    ShapeId char(36),
    PropertyName varchar(16) not null,
    PropartyValue Double not null,
    primary key (ShapeId, PropertyName),
    foreign key (ShapeId) references Shape(Uuid)
);